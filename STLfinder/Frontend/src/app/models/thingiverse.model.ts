export class ThingiverseModel {
    id: string;
    url: string;
    imgUrl: string;
    imgName: string;
    className: string

    constructor(id: string, url: string, imgUrl: string, imgName: string, className: string) {
        this.id = id;
        this.url = url;
        this.imgUrl = imgUrl;
        this.imgName = imgName;
        this.className = className;
    }
}
