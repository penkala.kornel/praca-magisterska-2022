export class Prediction {
    imgId: string;
    imgPath: string;
    distance: number;
    publicUrl: string;

    constructor(imgId: string, imgPath: string, distance: number, publicUrl: string) {
        this.imgId = imgId;
        this.imgPath = imgPath;
        this.distance = distance;
        this.publicUrl = publicUrl;
    }
}
