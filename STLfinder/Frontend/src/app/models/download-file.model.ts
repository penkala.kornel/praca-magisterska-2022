export class DownloadFile {
    url: string
    
    constructor(url: string) {
        this.url = url;
    }
}
