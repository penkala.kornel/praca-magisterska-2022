export class ThingiverseFile {
    id: string;
    img: string;
    name: string;
    downloadUrl: string;

    constructor(id: string, img: string, name: string, downloadUrl: string) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.downloadUrl = downloadUrl;
    }
}
