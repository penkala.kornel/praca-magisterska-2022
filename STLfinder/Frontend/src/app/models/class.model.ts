export class Class {
    className: string;
    isWinner: boolean;
    isActive: boolean;

    constructor(className: string, isWinner: boolean, isActive: boolean) {
        this.className = className;
        this.isWinner = isWinner;
        this.isActive = isActive;
    }
}
