import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperOrientation } from '@angular/cdk/stepper';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Class } from 'src/app/models/class.model';
import { Prediction } from 'src/app/models/prediction.model';
import { UploadedFile } from 'src/app/models/uploaded-file.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  @ViewChild('stepper')
  private stepper!: MatStepper;
  stepperOrientation: Observable<StepperOrientation>;
  predictedFiles: Prediction[] = [];
  predictedClass: Class[] = [];
  uploadId: string = "";

  constructor(breakpointObserver: BreakpointObserver, private apiService: ApiService) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({ matches }) => {
        return (matches ? 'horizontal' : 'vertical');
      }));
  }

  ngOnInit(): void { }

  onUploadFile(file: UploadedFile) {
    this.uploadId = file.id;
    this.getClasses(file);
  }

  onChooseClass(c: Class) {
    this.getTop5Results(this.uploadId, c.className);
  }

  getClasses(file: UploadedFile) {
    this.apiService.prediction(file.id).subscribe(res => {
      this.predictedClass = res;
      this.stepper.next();
    });
  }

  getTop5Results(fileId: string, className: string) {
    this.apiService.getTop5(fileId, className).subscribe(res => {
      this.predictedFiles = res;
    })
  }

  identify(index: number, item: any) {
    return item.imgId;
  }
}
