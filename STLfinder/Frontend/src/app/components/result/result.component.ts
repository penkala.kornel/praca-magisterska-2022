import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Prediction } from 'src/app/models/prediction.model';
import { ThingiverseFile } from 'src/app/models/thingiverse-file.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultComponent implements OnInit {
  _data!: Prediction;

  get data(): Prediction {
    return this._data;
  }

  @Input() set data(value: Prediction) {
    this._data = value;
    this.getResultFiles(value.imgId);
  }

  files !: Array<ThingiverseFile>
  loading: boolean = true;

  constructor(private apiService: ApiService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  getResultFiles(id: string) {
    this.apiService.getThingiverseProjectFiles(id.split("_")[0]).subscribe(res => {
      this.files = res;
      this.loading = false;
      this.changeDetectorRef.markForCheck();
    });
  }

  download(url: string) {
    var a = document.createElement("a");
    a.href = url;
    a.click();
  }

  goToUrl(url: string) {
    window.open(url);
  }
}
