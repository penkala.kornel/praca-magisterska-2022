import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';
import { UploadedFile } from 'src/app/models/uploaded-file.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {
  @ViewChild('fileDropRef')
  fileInputRef!: ElementRef;

  @Input() fileExtentions: string[] = [".png", ".jpg", ".jpeg"]
  @Output() uploadFile = new EventEmitter<UploadedFile>();

  constructor(private apiService: ApiService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  file: any;

  onFileDropped($event: any) {
    var extention = $event[0].name.split(".").pop();

    if (!this.fileExtentions.find(f => f.includes(extention))) {
      this._snackBar.open("Incorrect file extension. (Allowed: .jpg,.jpeg, .png)!", "Ok", {
        horizontalPosition: "right",
        verticalPosition: "top",
        panelClass: ['style-error'],
      });

      this.deleteFile();
      return;
    }
    
    this.prepareFile($event[0]);
  }

  fileBrowseHandler($event: any) {
    this.prepareFile($event.target.files[0]);
  }

  deleteFile() {
    this.file = null;
    this.fileInputRef.nativeElement.value = "";
  }

  prepareFile(file: any) {
    this.fileInputRef.nativeElement.value = "";
    file.progress = 0;
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      file.src = event.target?.result ?? "";
    }

    this.file = file;
    this.uploadToServer(file)
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes: number, decimals: number = 2) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  uploadToServer(file: any) {
    this.apiService.uploadFile(file).subscribe((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          var eventTotal = event.total ? event.total : 0;
          file.progress = Math.round(event.loaded / eventTotal * 100);
          break;
        case HttpEventType.Response:
          this._snackBar.open("Image upload successfully!", "", {
            horizontalPosition: "right",
            verticalPosition: "top",
            duration: 5000,
          });
          file.uploadId = event.body?.id;
      }
    })
  }

  next() {
    this.uploadFile.emit(new UploadedFile(this.file.uploadId));
  }
}
