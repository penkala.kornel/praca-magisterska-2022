import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Class } from 'src/app/models/class.model';

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.scss']
})
export class ClassListComponent implements OnInit {
  @Input() data = Array<Class>();
  @Output() chooseClass = new EventEmitter<Class>();
  constructor() { }

  ngOnInit(): void {
  }

  choose(c: Class) {
    this.data.map(d => d.isActive = false);
    c.isActive = true;
  }

  next() {
    var res = this.data.find(d => d.isActive);
    if(!res){
      res = this.data.find(d => d.isWinner);
    }
    this.chooseClass.emit(res);
  }

}
