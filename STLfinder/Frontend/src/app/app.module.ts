import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { FilePickerModule } from 'ngx-awesome-uploader';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';

import { AppComponent } from './app.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { ApiService } from './services/api.service';
import { StepperComponent } from './components/stepper/stepper.component';
import { ResultComponent } from './components/result/result.component';
import { DropzoneDirective } from './directives/dropzone.directive';
import { ClassListComponent } from './components/class-list/class-list.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadFileComponent,
    StepperComponent,
    ResultComponent,
    DropzoneDirective,
    ClassListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    LoggerModule.forRoot({ level: NgxLoggerLevel.DEBUG }),
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatStepperModule,
    FilePickerModule,
    MatInputModule,
    MatCardModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatExpansionModule,
    MatSelectModule,
    MatMenuModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
