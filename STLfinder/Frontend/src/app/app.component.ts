import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { first, take } from 'rxjs/operators';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'magisterka-front';
  allClasses!: string[];

  constructor(private apiService: ApiService) {
    this.getAllClasses();
  }

  goToMainPage() {
    window.location.reload();
  }

  getAllClasses() {
    this.apiService.getAllClasses().pipe(first()).subscribe(res => {
      this.allClasses = res;
    });
  }
}
