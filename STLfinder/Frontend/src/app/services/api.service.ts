import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Prediction } from '../models/prediction.model';
import { retry, catchError } from 'rxjs/operators';
import { ThingiverseFile } from '../models/thingiverse-file.model';
import { UploadedFile } from '../models/uploaded-file.model';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { DownloadFile } from '../models/download-file.model';
import { Class } from '../models/class.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = "http://127.0.0.1:5000/api"

  constructor(private http: HttpClient, public snackBar: MatSnackBar) {
  }

  uploadFile(file: File): Observable<HttpEvent<UploadedFile>> {
    const formData = new FormData();
    formData.append("file", file, file.name);
    return this.http.post<UploadedFile>(this.baseUrl + `/file/upload`, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      retry(1),
      catchError(err => { return this.handleError(err) })
    )
  }

  prediction(id: string): Observable<Array<Class>> {
    return this.http.get<Array<Class>>(this.baseUrl + `/prediction/${id}`)
      .pipe(
        retry(1),
        catchError(err => { return this.handleError(err) })
      )
  }

  getTop5(id: string, className:string): Observable<Array<Prediction>> {
    return this.http.get<Array<Prediction>>(this.baseUrl + `/prediction/${id}/top5/${className}`)
      .pipe(
        retry(1),
        catchError(err => { return this.handleError(err) })
      )
  }

  getThingiverseProjectFiles(id: string) {
    return this.http.get<Array<ThingiverseFile>>(this.baseUrl + `/thingiverse/${id}`)
      .pipe(
        retry(1),
        catchError(err => { return this.handleError(err) })
      )
  }

  downloadThingiverseFile(id: string) {
    return this.http.get<DownloadFile>(this.baseUrl + `/thingiverse/download/${id}`)
      .pipe(
        retry(1),
        catchError(err => { return this.handleError(err) })
      )
  }

  getAllClasses(): Observable<string[]> {
    return this.http.get<string[]>(this.baseUrl + "/prediction/classes").pipe(
      retry(1),
      catchError(err => { return this.handleError(err) })
    )
  }

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    console.log(errorMessage)

    this.snackBar.open('Something went wrong, please try again later.', "", this.configError);
    return throwError('Something went wrong, please try again later.');
  }

  private configError: MatSnackBarConfig = {
    panelClass: ['style-error'],
    horizontalPosition: "right",
    verticalPosition: "top",
    duration: 5000,
  };
}
