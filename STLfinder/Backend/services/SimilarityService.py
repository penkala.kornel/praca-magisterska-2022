from keras import Input
from keras.applications.resnet import ResNet50
from keras.layers import GlobalAveragePooling2D, Dense
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.models import Model
import numpy as np
from tensorflow.keras.applications.inception_v3 import InceptionV3, preprocess_input
import pickle
import pandas as pd
from scipy import spatial

from flask import current_app as app


class SimilarityService:

    def __init__(self):
        # self.getResnetModel()
        # self.getVgg16Model()
        self.imgSize = app.config["IMG_SIZE"]
        self.getInceptionV3Model()

    def getInceptionV3Model(self):
        self.model = InceptionV3(weights='imagenet', include_top=False, input_shape=(self.imgSize + (3,)),
                                 pooling='max')

    def getResnetModel(self):
        model = ResNet50(weights='imagenet', include_top=False,
                         input_shape=(self.imgSize + (3,)))
        input = Input(shape=(self.imgSize + (3,)))
        x = model(input)
        x = GlobalAveragePooling2D()(x)
        x = Dense(5, activation='softmax')(x)
        model_similarity_optimized = Model(inputs=input, outputs=x)
        model_similarity_optimized.layers.pop()
        self.model = Model(model_similarity_optimized.input,
                           model_similarity_optimized.layers[-1].output)

    def getVgg16Model(self):
        base_model = VGG16(weights='imagenet', input_shape=(self.imgSize + (3,)))
        self.model = Model(inputs=base_model.input, outputs=base_model.get_layer('fc1').output)

    def extract(self, img):
        img = img.resize(self.imgSize)
        img = img.convert('RGB')
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        feature = self.model.predict(x)
        return feature.flatten() / np.linalg.norm(feature)

    def getAllFeatures(self):
        fileNames = pickle.load(open(app.config['FEATURES_FILE_NAMES'], 'rb'))
        features = pickle.load(open(app.config['FEATURES'], 'rb'))
        return fileNames, features

    def getAllFeaturesByClassName(self, className):
        fileNames = pickle.load(open(app.config['FEATURES_FILE_NAMES'], 'rb'))
        features = pickle.load(open(app.config['FEATURES'], 'rb'))
        data = pd.read_csv(app.config['DATA_CSV'], delimiter=';')

        imageNames = []
        for index, item in enumerate(data[className]):
            if item == 1:
                imageNames.append(data.ImgName[index])

        filtered = []
        for index, score in enumerate(fileNames):
            if str(score.split("\\")[1]) in imageNames:
                filtered.append(index)

        return [fileNames[i] for i in filtered], [features[i] for i in filtered]

    def findSimilar(self, className, img, k=5):
        query = self.extract(img)
        features = self.getAllFeaturesByClassName(className)

        similarities = []
        for filename, encoding in list(zip(*features)):
            h_distance = spatial.distance.hamming(query, encoding)
            c_distance = spatial.distance.cosine(query, encoding)
            similarity = 1 - (h_distance + c_distance) / 2
            f = filename.split("\\")
            similarities.append((f[1], similarity))

        similarities.sort(key=lambda tup: -tup[1])

        response = []
        for res in similarities[:k]:
            imgId, imgPath, publicUrl = self.getRowFromData(res[0])
            response.append({
                'distance': str(res[1]),
                'imgPath': str(imgPath),
                'imgId': str(imgId),
                'publicUrl': str(publicUrl)
            })
        return response

    def getRowFromData(self, fileName):
        data = pd.read_csv(app.config['DATA_CSV'], delimiter=';')
        for index, item in enumerate(data.ImgName):
            if item == fileName:
                return data.Id[index], data.ImgUrl[index], data.ThingUrl[index]
