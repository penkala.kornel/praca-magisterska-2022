import requests
from flask import current_app as app
import json

from models.models import ThingiverseFile


class ThingiverseService:
    def __init__(self):
        self.baseUrl = app.config['THINGIVERSE_BASE_URL']
        self.headers = {"Authorization": "Bearer {0}".format(app.config['THINGIVERSE_APP_KEY'])}

    def buildApiUrl(self, url):
        return f"{self.baseUrl}/{url}"

    def downloadFile(self, fileId):
        if not fileId:
            return

        res = requests.get(self.buildApiUrl(f"files/{fileId}/download"), headers=self.headers)
        if res.ok:
            return res.url
        return

    def getThingFiles(self, thingId):
        if not thingId:
            return []

        res = requests.get(self.buildApiUrl(f"things/{thingId}/files/"), headers=self.headers)
        if res.ok:
            return [ThingiverseFile(r['id'], r['name'], r['thumbnail'], r['direct_url']) for r in json.loads(res.text)]
        return []
