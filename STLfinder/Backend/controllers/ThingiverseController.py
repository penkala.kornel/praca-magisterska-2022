from flask import Blueprint, jsonify
from flask import current_app as app

api = Blueprint('thingiverse', __name__)


@api.route("/<string:id>", methods=["GET"])
def getThingProjectFiles(id):
    if not id:
        return "Thing ID cannot be empty.", 400
    return jsonify([e.to_json() for e in app.thingiverseService.getThingFiles(id)])


@api.route("/download/<string:id>", methods=["GET"])
def downloadFile(id):
    if not id:
        return "Thing ID cannot be empty.", 400
    downloadUrl = app.thingiverseService.downloadFile(id)
    if not downloadUrl:
        return "File not exist.", 400

    return jsonify({'url': downloadUrl})
