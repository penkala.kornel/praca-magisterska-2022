import os
import numpy as np
import pandas as pd
import tensorflow as tf
from flask import Blueprint
from flask import jsonify
from tensorflow import keras
from flask import current_app as app


api = Blueprint('prediction', __name__)


@api.route("/classes", methods=["GET"])
def getClasses():
    classNames = getAllClasses()
    return jsonify(list(classNames.values()))


@api.route("/<string:id>", methods=["GET"])
def predict(id):
    filePath = app.config['UPLOAD_FOLDER'] + id + ".png"
    if not os.path.isfile(filePath):
        return "File not exist", 400

    classNames = getAllClasses()
    model = keras.models.load_model(app.config['VGG16_MODEL'])
    img = tf.keras.utils.load_img(filePath, target_size=app.config['IMG_SIZE'])
    imgArray = tf.keras.utils.img_to_array(img)
    imgArray = tf.expand_dims(imgArray, 0)
    prediction = model.predict(imgArray)

    res = []
    for i in classNames:
        res.append({
            "className": classNames[i],
            "isWinner": bool(i == np.argmax(prediction))
        })

    return jsonify(res)


@api.route("/<string:id>/top5/<string:className>", methods=["GET"])
def getTop5(id, className):
    filePath = app.config['UPLOAD_FOLDER'] + id + ".png"
    if not os.path.isfile(filePath):
        return "File not exist", 400

    classNames = getAllClasses()
    if className not in classNames.values():
        return "Class not exist!", 400

    img = tf.keras.utils.load_img(filePath, target_size=app.config['IMG_SIZE'])
    response = app.similarityService.findSimilar(className, img)
    return jsonify(response)


def getAllClasses():
    data = pd.read_csv(app.config['DATA_CSV'], delimiter=';')
    classNames = data.columns[4:]
    return dict(zip(range(len(classNames)), classNames))
