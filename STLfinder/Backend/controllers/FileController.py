import os
import uuid
from PIL import Image
from flask import Blueprint
from flask import current_app as app
from flask import request

api = Blueprint('file', __name__)


@api.route("/upload", methods=["POST"])
def upload():
    file = request.files['file']
    if not file or file.filename == '':
        return "File cannot be empty.", 400

    fileExt = os.path.splitext(file.filename)[1]
    if fileExt not in app.config['UPLOAD_EXTENSIONS']:
        return "Incorrect file extension. (Allowed: .jpg,.jpeg, .png)", 400

    uploadId = str(uuid.uuid4())
    im = Image.open(file)
    imResize = im.resize(app.config['IMG_SIZE'], Image.ANTIALIAS)
    imResize.save(os.path.join(app.config['UPLOAD_FOLDER'], '%s.png' % uploadId), 'PNG', quality=95)
    return {'id': uploadId}
