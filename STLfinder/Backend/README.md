## Installation

Install with pip:

```
$ pip install -r requirements.txt
```

## Getting started


**The first change you should make** is to set the `THINGIVERSE_APP_KEY` variable in `config.py` to your [Thingiverse Developer](https://www.thingiverse.com/developers/getting-started) key.

**The next thing you need to do** is download the necessary files from [link](https://drive.google.com/file/d/1oYQOHuhYb8BUPkN5F-5_2XrphV-64qQL/view?usp=sharing) and unpack them to the /resources.