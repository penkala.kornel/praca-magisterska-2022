import os

SECRET_KEY = os.urandom(32)

# Grabs the folder where the script runs.
basedir = os.path.abspath(os.path.dirname(__file__))

# Enable debug mode.
DEBUG = True

# Thingiverse.com
THINGIVERSE_APP_KEY = ""
THINGIVERSE_BASE_URL = "https://api.thingiverse.com"

CORS_HEADERS = "Content-Type"

UPLOAD_FOLDER = 'resources/uploads/'
VGG16_MODEL = 'resources/VGG16.h5'
DATA_CSV = 'resources/data.csv'
FEATURES = 'resources/Features.pickle'
FEATURES_FILE_NAMES = 'resources/FileNames.pickle'

UPLOAD_EXTENSIONS = ['.jpg', '.png', '.jpeg']
IMG_SIZE = (224, 224)
