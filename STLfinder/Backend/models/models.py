class ThingiverseFile:
    def __init__(self, id, name, img, downloadUrl):
        self.id = id
        self.name = name
        self.img = img
        self.downloadUrl = downloadUrl

    def to_json(self):
        return {
            "id": self.id,
            "img": self.img,
            "name": self.name,
            "downloadUrl": self.downloadUrl
        }
