from flask_cors import CORS
from flask import Flask
import os
from controllers import FileController, PredictionController, ThingiverseController
from services.SimilarityService import SimilarityService
from services.ThingiverseService import ThingiverseService

app = Flask(__name__, static_folder='resources')
app.config.from_object('config')
app.register_blueprint(FileController.api, url_prefix='/api/file')
app.register_blueprint(PredictionController.api, url_prefix='/api/prediction')
app.register_blueprint(ThingiverseController.api, url_prefix='/api/thingiverse')
cors = CORS(app)

if __name__ == '__main__':
    app.debug = app.config['DEBUG']
    with app.app_context():
        app.similarityService = SimilarityService()
        app.thingiverseService = ThingiverseService()

    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.makedirs(app.config['UPLOAD_FOLDER'])

    app.run(debug=app.debug)
