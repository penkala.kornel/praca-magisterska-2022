import os
import pickle
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.layers import GlobalAveragePooling2D, Dense
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras import Input, Model
from scipy import spatial
import numpy as np
from tensorflow.keras.preprocessing import image
from tqdm import tqdm
from numpy.linalg import norm

imgSize = (224, 224)
imgShape = (224, 224) + (3,)


def getAllFiles(imagesPath):
    files = []

    for root, dirs, fileNames in os.walk(imagesPath):
        for fileName in tqdm(fileNames):
            files.append(os.path.join(root, fileName))
    return sorted(files)


def getAllFeatures(featureFileName, fileNames):
    fileNames = pickle.load(open(fileNames, 'rb'))
    features = pickle.load(open(featureFileName, 'rb'))
    return fileNames, features


def plotFeatures(featureFileName, fileNames, numOfFeatureDimensions=100):
    filenames, features = getAllFeatures(featureFileName, fileNames)

    pca = PCA(n_components=numOfFeatureDimensions)
    pca.fit(features)
    feature_list_compressed = pca.transform(features)

    tsne = TSNE(n_components=2, verbose=1, n_iter=4000, metric='cosine', init='pca')
    tsne_results = tsne.fit_transform(feature_list_compressed)
    tsne_results = StandardScaler().fit_transform(tsne_results)

    size = (45, 45)
    imgs = [img_to_array(load_img(path, target_size=size)) / 255 for path in filenames]

    fig, ax = plt.subplots(figsize=size)
    artist = []
    for xy, i in tqdm(zip(tsne_results, imgs)):
        x, y = xy
        img = OffsetImage(i, zoom=0.6)
        ab = AnnotationBbox(img, (x, y), xycoords='data', frameon=False)
        artist.append(ax.add_artist(ab))
    ax.update_datalim(tsne_results)
    ax.autoscale()
    ax.axis('off')
    plt.tight_layout(pad=1.2)
    plt.show()


def plotPCA(featureFileName, fileNames, numOfFeatureDimensions=100):
    filenames, features = getAllFeatures(featureFileName, fileNames)

    pca = PCA(n_components=numOfFeatureDimensions)
    pca.fit(features)
    plt.plot(range(1, numOfFeatureDimensions + 1), pca.explained_variance_ratio_, 'o--', markersize=4)
    plt.title("Variance for each PCA dimension")
    plt.xlabel('PCA Dimensions')
    plt.ylabel('Variance')
    plt.show()


def getResNet50Model():
    model = ResNet50(weights='imagenet', include_top=False,
                     input_shape=imgShape)
    input = Input(shape=imgShape)
    x = model(input)
    x = GlobalAveragePooling2D()(x)
    x = Dense(5, activation='softmax')(x)
    model_similarity_optimized = Model(inputs=input, outputs=x)
    model_similarity_optimized.layers.pop()
    model = Model(model_similarity_optimized.input, model_similarity_optimized.layers[-2].output)
    return model


def getVGG16Model():
    modelVGG16 = VGG16(weights='imagenet', input_shape=imgShape, pooling='max')
    model = Model(inputs=modelVGG16.input, outputs=modelVGG16.get_layer("fc2").output)
    return model


def getInceptionV3Model():
    return InceptionV3(weights='imagenet', include_top=False, input_shape=imgShape, pooling='max')


def extractFeaturesFromImg(imgPath, model):
    img = image.load_img(imgPath, target_size=imgSize)
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    f = model.predict(x)
    return f.flatten() / norm(f.flatten())


def generateFeatures(path, featureFileName, fileNames, model):
    files = getAllFiles(path)
    features = []

    for fileName in tqdm(files):
        features.append(extractFeaturesFromImg(fileName, model))

    pickle.dump(files, open(fileNames + ".pickle", 'wb'))
    pickle.dump(features, open(featureFileName + ".pickle", 'wb'))

    return files, features


def plotSimilarImages(testImgPaths, featureFileName, fileNames, model, max_query_imgs=7, k=5):
    testImgPaths = testImgPaths[:min(max_query_imgs, len(testImgPaths))]
    fig, axs = plt.subplots(len(testImgPaths), k + 1, figsize=(10, 10))

    for i in tqdm(range(len(testImgPaths))):
        imgPath = testImgPaths[i]
        similar = findSimilarImagesPaths(imgPath, featureFileName, fileNames, model, k)
        axs[i][0].set_title('Query image', size=7)
        axs[i][0].imshow(img_to_array(load_img(imgPath)) / 255)
        axs[i][0].axis('off')
        axs[i][0].autoscale()

        cnt = 1
        for path, similarity in similar:
            axs[i][cnt].imshow(img_to_array(load_img(path)) / 255)
            axs[i][cnt].set_title('Related image\n similarity %f' % (similarity,), size=7)
            axs[i][cnt].axis('off')
            axs[i][cnt].autoscale()
            cnt += 1

    plt.tight_layout(h_pad=2)
    plt.show()


def findSimilarImagesPaths(imgPath, featureFileName, fileNames, model, k=5):
    imgFeatures = extractFeaturesFromImg(imgPath, model)
    features = getAllFeatures(featureFileName, fileNames)

    similarities = []

    for filename, encoding in list(zip(*features)):
        h_distance = spatial.distance.hamming(imgFeatures, encoding)
        c_distance = spatial.distance.cosine(imgFeatures, encoding)
        similarity = 1 - (h_distance + c_distance) / 2
        similarities.append((filename, similarity))

    similarities.sort(key=lambda tup: -tup[1])
    return similarities[:k]


models = [
    # ["ResNet50", "getResNet50Model"],
    # ["VGG16", "getVGG16Model"],
    ["InceptionV3", "getInceptionV3Model"]
]

experimentsPath = os.path.abspath(os.path.join(os.getcwd(), os.pardir, "Experiments"))
imgPath = os.path.join(experimentsPath, "ImagesAll")
testImgPath = os.path.join(experimentsPath, "Tests")

for m in models:
    model = globals()[m[1]]()
    print(m[0])
    model.summary()
    generateFeatures(imgPath, f"Features{m[0]}", f"FileNames{m[0]}", model)
    plotPCA(f"Features{m[0]}.pickle", f"FileNames{m[0]}.pickle", 5)
    paths = getAllFiles(testImgPath)
    plotSimilarImages(paths, f"Features{m[0]}.pickle", f"FileNames{m[0]}.pickle", model)
