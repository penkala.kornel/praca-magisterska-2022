import os
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import accuracy_score, f1_score, precision_score, confusion_matrix
import pandas as pd
import itertools
from tensorflow.keras.optimizers import SGD, Adam

experimentsPath = os.path.abspath(os.path.join(os.getcwd(), os.pardir, "Experiments"))
imagePath = os.path.join(experimentsPath, "Images")

imgWidth = 224
imgHeight = 224
batchSize = 32

trainDs = tf.keras.utils.image_dataset_from_directory(
    imagePath,
    validation_split=0.2,
    subset="training",
    seed=123,
    image_size=(imgHeight, imgWidth),
    batch_size=batchSize,
    label_mode='categorical')

valDs = tf.keras.utils.image_dataset_from_directory(
    imagePath,
    validation_split=0.2,
    subset="validation",
    seed=123,
    image_size=(imgHeight, imgWidth),
    batch_size=batchSize,
    label_mode='categorical')

classNames = trainDs.class_names
print(classNames)

quantity_tr = {}
for folder in os.listdir(imagePath):
    quantity_tr[folder] = len(os.listdir(imagePath + '\\' + folder))

quantity_train = pd.DataFrame(list(quantity_tr.items()), index=range(0, len(quantity_tr)), columns=['class', 'count'])

figure, ax = plt.subplots(1, 2, figsize=(20, 5))
sns.barplot(x='class', y='count', data=quantity_train, ax=ax[0])

AUTOTUNE = tf.data.AUTOTUNE

trainDs = trainDs.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
valDs = valDs.cache().prefetch(buffer_size=AUTOTUNE)


def generateAccuracyPlot(history, name):
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(len(acc))

    plt.figure(figsize=(18, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs, acc, label='Training Accuracy')
    plt.plot(epochs, val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs, loss, label='Training Loss')
    plt.plot(epochs, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.savefig(os.getcwd() + "/" + name + ".png")
    plt.show()


def generateReport(y_true, y_pred, name):
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred, average='weighted')
    f1Score = f1_score(y_true, y_pred, average='weighted')
    print("Accuracy  : {}".format(accuracy))
    print("Precision : {}".format(precision))
    print("f1Score : {}".format(f1Score))

    plt.figure(figsize=(7, 7))
    cm = confusion_matrix(y_true, y_pred)
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.viridis)
    plt.title('Confusion Matrix')
    plt.colorbar()
    tick_marks = np.arange(len(classNames))
    plt.xticks(tick_marks, classNames, rotation=45)
    plt.yticks(tick_marks, classNames)
    thresh = cm.max() * 0.8
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="black" if cm[i, j] > thresh else "white")
        pass

    plt.ylabel('True Label')
    plt.xlabel('Predicted Label')
    plt.figtext(0.05, 0.00, f"Accuracy = {accuracy} \n Precision = {precision} \n f1Score = {f1Score}", fontsize=12,
                va="top", ha="left")
    plt.savefig(os.getcwd() + "/" + name + ".png")
    plt.show()



optSGD = SGD(lr=0.001, momentum=0.7)
optAdam = Adam(lr=0.001)


def getCustomModel1SGD():
    model = Sequential([
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optSGD,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


def getCustomModel1Adam():
    model = Sequential([
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optAdam,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


dataAugmentation = tf.keras.Sequential(
    [
        layers.RandomFlip("horizontal",
                          input_shape=(imgHeight,
                                       imgWidth,
                                       3)),
        layers.RandomZoom(0.4),
    ]
)


def getCustomModel1SGDWithAugmentation():
    model = Sequential([
        dataAugmentation,
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),
        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optSGD,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


def getCustomModel1AdamWithAugmentation():
    model = Sequential([
        dataAugmentation,
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),

        layers.Conv2D(16, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),

        layers.Conv2D(32, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),

        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(),
        layers.Flatten(),

        layers.Dense(128, activation='relu'),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optAdam,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


def getCustomModel2SGD():
    model = Sequential([
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),

        layers.Conv2D(32, (3, 3), input_shape=(imgWidth, imgHeight, 3), padding='same', activation='relu'),

        layers.Conv2D(32, 3, input_shape=(imgWidth, imgHeight, 3), activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(128, 3, activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Flatten(),
        layers.Dropout(0.2),

        layers.Dense(32, activation='relu'),
        layers.Dropout(0.3),
        layers.BatchNormalization(),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optSGD,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


def getCustomModel2Adam():
    model = Sequential([
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),
        layers.Conv2D(32, (3, 3), input_shape=(imgWidth, imgHeight, 3), padding='same', activation='relu'),

        layers.Conv2D(32, 3, input_shape=(imgWidth, imgHeight, 3), activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(128, 3, activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Flatten(),
        layers.Dropout(0.2),

        layers.Dense(32, activation='relu'),
        layers.Dropout(0.3),
        layers.BatchNormalization(),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optAdam,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


def getCustomModel2SGDWithAugmentation():
    model = Sequential([
        dataAugmentation,
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),
        layers.Conv2D(32, (3, 3), input_shape=(imgWidth, imgHeight, 3), padding='same', activation='relu'),

        layers.Conv2D(32, 3, input_shape=(imgWidth, imgHeight, 3), activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(128, 3, activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Flatten(),
        layers.Dropout(0.2),

        layers.Dense(32, activation='relu'),
        layers.Dropout(0.3),
        layers.BatchNormalization(),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optSGD,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


def getCustomModel2AdamWithAugmentation():
    model = Sequential([
        dataAugmentation,
        layers.Rescaling(1. / 255, input_shape=(imgHeight, imgWidth, 3)),
        layers.Conv2D(32, (3, 3), input_shape=(imgWidth, imgHeight, 3), padding='same', activation='relu'),

        layers.Conv2D(32, 3, input_shape=(imgWidth, imgHeight, 3), activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(64, 3, activation='relu', padding='same'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Conv2D(128, 3, activation='relu', padding='same'),
        layers.Dropout(0.2),
        layers.BatchNormalization(),

        layers.Flatten(),
        layers.Dropout(0.2),

        layers.Dense(32, activation='relu'),
        layers.Dropout(0.3),
        layers.BatchNormalization(),
        layers.Dense(len(classNames), activation='softmax')
    ])

    model.compile(optimizer=optAdam,
                  loss="categorical_crossentropy",
                  metrics=['accuracy'])
    return model


plt.figure(figsize=(10, 10))
for images, _ in trainDs.take(1):
    for i in range(9):
        augmented_images = dataAugmentation(images, training=True)
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(augmented_images[0].numpy().astype("uint8"))
        plt.axis("off")

models = [
    ["CustomModel1_SGD", "getCustomModel1SGD", 100],
    ["CustomModel1_Adam", "getCustomModel1Adam", 100],
    ["CustomModel1_SGD_Aug", "getCustomModel1SGDWithAugmentation", 100],
    ["CustomModel1_Adam_Aug", "getCustomModel1AdamWithAugmentation", 100],
    ["CustomModel2_SGD", "getCustomModel2SGD", 100],
    ["CustomModel2_Adam", "getCustomModel2Adam", 100],
    ["CustomModel2_SGD_Aug", "getCustomModel2SGDWithAugmentation", 100],
    ["CustomModel2_Adam_Aug", "getCustomModel2AdamWithAugmentation", 100]
]

for model in models:
    modelValue = locals()[model[1]]()
    modelValue.summary()
    callbacks = [tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=8),
                 tf.keras.callbacks.ModelCheckpoint(filepath=f'{model[0]}_model.h5', monitor='val_loss',
                                                    save_best_only=True)]
    history = modelValue.fit(trainDs, validation_data=valDs, epochs=model[2], callbacks=[callbacks])
    generateAccuracyPlot(history, f"{model[0]}_Accuracy")

    labels = np.array([])
    predictions = modelValue.predict(valDs, verbose=1)
    predictions = list(map(lambda y: np.argmax(y), predictions))
    for x, y in valDs:
        labels = np.concatenate([labels, np.argmax(y.numpy(), axis=-1)])

    generateReport(labels, predictions, f"{model[0]}_ConfusionMatrix")


