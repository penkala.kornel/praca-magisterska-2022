import os
from sklearn.model_selection import StratifiedKFold
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
import pandas as pd
from tensorflow.keras.optimizers import SGD, Adam
import gc

experimentsPath = os.path.abspath(os.path.join(os.getcwd(), os.pardir, "Experiments"))
imgPath = os.path.join(experimentsPath, "ImagesAll")

trainData = pd.read_csv(os.path.join(experimentsPath, 'data_csv_string.csv'), sep=";")
trainData.head()

trainY = trainData.label
trainX = trainData.drop(['label'], axis=1)
print(trainY)
print(trainX)

imgWidth = 224
imgHeight = 224

optSGD = SGD(learning_rate=0.0001, momentum=0.7)
optAdam = Adam(learning_rate=0.0001)

dataAugmentation = keras.Sequential(
    [
        layers.RandomFlip("horizontal",
                          input_shape=(imgHeight,
                                       imgWidth,
                                       3)),
        layers.RandomZoom(0.4),
    ]
)

globalAverageLayer = layers.GlobalAveragePooling2D()
predictionLayer = tf.keras.layers.Dense(5, activation='softmax')


def getVGG16ModelSGD():
    base_model = tf.keras.applications.VGG16(weights='imagenet', include_top=False,
                                             input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.vgg16.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optSGD, metrics=['accuracy'])
    return model


def getVGG16ModelAdam():
    base_model = tf.keras.applications.VGG16(weights='imagenet', include_top=False,
                                             input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.vgg16.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optAdam, metrics=['accuracy'])
    return model


def getInceptionV3ModelSGD():
    base_model = tf.keras.applications.InceptionV3(weights='imagenet', include_top=False,
                                                   input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.inception_v3.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optSGD, metrics=['accuracy'])
    return model


def getInceptionV3ModelAdam():
    base_model = tf.keras.applications.InceptionV3(weights='imagenet', include_top=False,
                                                   input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.inception_v3.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optAdam, metrics=['accuracy'])
    return model


def getResNetModelSGD():
    base_model = tf.keras.applications.ResNet50(weights='imagenet', include_top=False,
                                                input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.resnet50.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(optimizer=optSGD, loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def getResNetModelAdam():
    base_model = tf.keras.applications.ResNet50(weights='imagenet', include_top=False,
                                                input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.resnet50.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optAdam, metrics=['accuracy'])
    return model


def writeReportToFile(modelName, acc, loss):
    with open('results.txt', 'a') as fp:
        fp.write(f'Model name: {modelName}\n')
        fp.write('------------------------------------------------------------------------\n')
        fp.write('Score per fold\n')
        for i in range(0, len(acc)):
            fp.write('------------------------------------------------------------------------\n')
            fp.write(f'> Fold {i + 1} - Loss: {loss[i]} - Accuracy: {acc[i]}%\n')
        fp.write('------------------------------------------------------------------------\n')
        fp.write('Average scores for all folds:\n')
        fp.write(f'> Accuracy: {np.mean(acc)} (+- {np.std(acc)})\n')
        fp.write(f'> Loss: {np.mean(loss)}\n')
        fp.write('------------------------------------------------------------------------\n\n')


def startKFold(modelName, function, epochs, batchSize=16, k=10):
    trainDg = ImageDataGenerator()
    validationDg = ImageDataGenerator()
    accuracyPerFold = []
    lossPerFold = []
    foldNumber = 0
    kfold = StratifiedKFold(n_splits=k, shuffle=True, random_state=42)

    for train_idx, val_idx in list(kfold.split(trainX, trainY)):
        try:
            modelValue = globals()[function]()
            xTrainDf = trainData.iloc[train_idx]
            xValidDf = trainData.iloc[val_idx]
            foldNumber += 1

            trainingDf = trainDg.flow_from_dataframe(dataframe=xTrainDf, directory=imgPath,
                                                     x_col="fileName", y_col="label",
                                                     class_mode="categorical",
                                                     target_size=(imgHeight, imgWidth),
                                                     batch_size=batchSize)

            validationDf = validationDg.flow_from_dataframe(dataframe=xValidDf, directory=imgPath,
                                                            x_col="fileName", y_col="label",
                                                            class_mode="categorical",
                                                            target_size=(imgHeight, imgWidth),
                                                            batch_size=batchSize)

            callbacks = [tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3)]

            modelValue.fit(trainingDf, validation_data=validationDf, epochs=epochs,
                           callbacks=[callbacks])

            scores = modelValue.evaluate(validationDf)
            print(
                f'Score for fold {foldNumber}: {modelValue.metrics_names[0]} of {scores[0]}; {modelValue.metrics_names[1]} of {scores[1] * 100}%')
            accuracyPerFold.append(scores[1] * 100)
            lossPerFold.append(scores[0])

            del modelValue
            gc.collect()
        except Exception as e:
            print(e)

    writeReportToFile(modelName, accuracyPerFold, lossPerFold)


startKFold("VGG16ModelSGD", "getVGG16ModelSGD", 50)
# startKFold("VGG16ModelAdam","getVGG16ModelAdam", 50)
# startKFold("InceptionV3ModelSGD","getInceptionV3ModelSGD", 50)
startKFold("InceptionV3ModelAdam", "getInceptionV3ModelAdam", 50)
startKFold("ResNetModelSGD", "getResNetModelSGD", 50)
# startKFold("ResNetModelAdam","getResNetModelAdam", 50)
