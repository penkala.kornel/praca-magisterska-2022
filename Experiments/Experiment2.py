import os
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, f1_score, precision_score, confusion_matrix
import itertools
from tensorflow.keras.optimizers import SGD, Adam


experimentsPath = os.path.abspath(os.path.join(os.getcwd(), os.pardir, "Experiments"))
imagePath = os.path.join(experimentsPath, "Images")

imgWidth = 224
imgHeight = 224
batchSize = 32

trainDs = tf.keras.utils.image_dataset_from_directory(
    imagePath,
    validation_split=0.2,
    subset="training",
    seed=123,
    image_size=(imgHeight, imgWidth),
    batch_size=batchSize,
    label_mode='categorical')

valDs = tf.keras.utils.image_dataset_from_directory(
    imagePath,
    validation_split=0.2,
    subset="validation",
    seed=123,
    image_size=(imgHeight, imgWidth),
    batch_size=batchSize,
    label_mode='categorical')

classNames = trainDs.class_names
print(classNames)

AUTOTUNE = tf.data.AUTOTUNE

trainDs = trainDs.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
valDs = valDs.cache().prefetch(buffer_size=AUTOTUNE)


def generateAccuracyPlot(history, name):
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(len(acc))

    plt.figure(figsize=(18, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs, acc, label='Training Accuracy')
    plt.plot(epochs, val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs, loss, label='Training Loss')
    plt.plot(epochs, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.savefig(os.getcwd() + "/" + name + ".png")
    plt.show()


def generateReport(y_true, y_pred, name):
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred, average='weighted')
    f1Score = f1_score(y_true, y_pred, average='weighted')
    print("Accuracy  : {}".format(accuracy))
    print("Precision : {}".format(precision))
    print("f1Score : {}".format(f1Score))

    plt.figure(figsize=(7, 7))
    cm = confusion_matrix(y_true, y_pred)
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.viridis)
    plt.title('Confusion Matrix')
    plt.colorbar()
    tick_marks = np.arange(len(classNames))
    plt.xticks(tick_marks, classNames, rotation=45)
    plt.yticks(tick_marks, classNames)
    thresh = cm.max() * 0.8
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="black" if cm[i, j] > thresh else "white")
        pass

    plt.ylabel('True Label')
    plt.xlabel('Predicted Label')
    plt.figtext(0.05, 0.00, f"Accuracy = {accuracy} \n Precision = {precision} \n f1Score = {f1Score}", fontsize=12,
                va="top", ha="left")
    plt.savefig(os.getcwd() + "/" + name + ".png")
    plt.show()



optSGD = SGD(learning_rate=0.0001, momentum=0.7)
optAdam = Adam(learning_rate=0.0001)

dataAugmentation = keras.Sequential(
    [
        layers.RandomFlip("horizontal",
                          input_shape=(imgHeight,
                                       imgWidth,
                                       3)),
        layers.RandomZoom(0.4),
    ]
)

globalAverageLayer = layers.GlobalAveragePooling2D()
predictionLayer = tf.keras.layers.Dense(len(classNames), activation='softmax')


def getVGG16ModelSGD():
    base_model = tf.keras.applications.VGG16(weights='imagenet', include_top=False,
                                             input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.vgg16.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optSGD, metrics=['accuracy'])
    return model


def getVGG16ModelAdam():
    base_model = tf.keras.applications.VGG16(weights='imagenet', include_top=False,
                                             input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.vgg16.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optAdam, metrics=['accuracy'])
    return model


def getInceptionV3ModelSGD():
    base_model = tf.keras.applications.InceptionV3(weights='imagenet', include_top=False,
                                                   input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.inception_v3.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optSGD, metrics=['accuracy'])
    return model


def getInceptionV3ModelAdam():
    base_model = tf.keras.applications.InceptionV3(weights='imagenet', include_top=False,
                                                   input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.inception_v3.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optAdam, metrics=['accuracy'])
    return model


def getResNetModelSGD():
    base_model = tf.keras.applications.ResNet50(weights='imagenet', include_top=False,
                                                input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.resnet50.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(optimizer=optSGD, loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def getResNetModelAdam():
    base_model = tf.keras.applications.ResNet50(weights='imagenet', include_top=False,
                                                input_shape=(imgHeight, imgWidth, 3))
    inputs = tf.keras.Input(shape=(imgHeight, imgWidth, 3))
    x = dataAugmentation(inputs)
    x = tf.keras.applications.resnet50.preprocess_input(x)
    x = base_model(x, training=False)
    x = globalAverageLayer(x)
    x = layers.Dense(1024, activation='relu')(x)
    x = layers.Dropout(0.5)(x)
    outputs = predictionLayer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(loss='categorical_crossentropy', optimizer=optAdam, metrics=['accuracy'])
    return model


models = [
    ["VGG16ModelSGD", "getVGG16ModelSGD", 50],
    ["VGG16ModelAdam", "getVGG16ModelAdam", 50],
    ["InceptionV3ModelSGD", "getInceptionV3ModelSGD", 50],
    ["InceptionV3ModelAdam", "getInceptionV3ModelAdam", 50],
    ["ResNetModelSGD", "getResNetModelSGD", 50],
    ["ResNetModelAdam", "getResNetModelAdam", 50],
]

for m in models:
    modelValue = locals()[m[1]]()
    modelValue.summary()
    callbacks = [tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5),
                 tf.keras.callbacks.ModelCheckpoint(filepath=f'{m[0]}_model.h5', monitor='val_loss',
                                                    save_best_only=True)]
    history = modelValue.fit(trainDs, validation_data=valDs, epochs=m[2], callbacks=[callbacks])
    generateAccuracyPlot(history, f"{m[0]}_Accuracy")

    labels = np.array([])
    predictions = modelValue.predict(valDs, verbose=1)
    predictions = list(map(lambda y: np.argmax(y), predictions))
    for x, y in valDs:
        labels = np.concatenate([labels, np.argmax(y.numpy(), axis=-1)])

    generateReport(labels, predictions, f"{m[0]}_ConfusionMatrix")
